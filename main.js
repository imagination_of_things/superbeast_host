// const Two = require("two.js");


$(function () {
  noise.seed(Math.random());

  var two = new Two({
    fullscreen: false,
    width: window.innerWidth,
    height: window.innerHeight,
    autostart: true
  }).appendTo(document.querySelector(".container"));
  let origin = new Two.Vector(window.innerWidth / 2, window.innerHeight / 2)
  let mouseCircle = two.makeCircle(0, 0, 20)

  var svg = document.querySelector("svg#superbeast");
  svg.style.display = "none";

  // MOUSE
  var delta = new Two.Vector();
  var mouse = new Two.Vector();
  var drag = 0.33;
  var radius = 50;

  var shadow = two.makeCircle(two.width / 2, two.height / 2, radius, 32);
  shadow.noStroke().fill = 'rgba(0, 0, 0, 0.2)';
  shadow.offset = new Two.Vector(-radius / 2, radius * 2);
  shadow.scale = 0.85;

  var ball = two.makeCircle(two.width / 2, two.height / 2, radius, 32);
  ball.noStroke().fill = 'red';

  ball.vertices.forEach(function (v) {
    v.origin = new Two.Vector().copy(v);
  });

  // 
  var foreground = two.makeGroup();

  var logo = two.interpret(svg);
  logo.center();

  // foreground.add(logo)
  console.log(logo.position)
  // foreground.c

  // console.log(logo)

  var rect = logo.getBoundingClientRect();

  let hairSystems = []
  ///////////////////////////////////////////////////
  /// Take the logo and parse out the hairy regions//
  ///////////////////////////////////////////////////
  console.log("lgogos")

  for (let i = 0; i < logo.children.length; i++) {

    //  Check if it is a hairs patch
    if (logo.children[i].id.includes("hairs")) {
      console.log(logo.children[i])


      if (logo.children[i].children[0]) {
        let hairsystem = new ParticleSystem(two, logo.children[i])
        hairSystems.push(hairsystem)
        // logo.children[i].opacity = 
      }


    }
  }








  // Scene resize
  two.bind("resize", function () {
    two.scene.translation.set(two.width / 2, two.height / 2);
    var aspect = two.width / two.height;

    // if (aspect >= 1) {
    //   two.scene.scale = rect.width / two.width;
    // } else {
    //   two.scene.scale = rect.height / two.height;
    // }
  });
  two.trigger("resize");










  // UPDATE

  var duration = 5000;
  var elapsed = 0;
  var above = true;
  var amplitude = 1;

  two.bind("update", function (frameCount, timeDelta) {
    if (!timeDelta) {
      return;
    }

    elapsed += timeDelta;
    var t = (elapsed % duration) / duration;
    var revolutions = Math.floor(elapsed / duration);
    above = !!(revolutions % 2);

    if (!(frameCount % 2)) {
      return;
    }


    for (let i = 0; i < hairSystems.length; i++) {
      hairSystems[i].run(mouse, elapsed);
    }
    delta.copy(mouse).subSelf(ball.translation);

    ball.vertices.forEach(function (v, i) {

      var dist = v.origin.distanceTo(delta);
      var pct = dist / radius;

      var x = delta.x * pct;
      var y = delta.y * pct;

      var destx = v.origin.x - x;
      var desty = v.origin.y - y;

      v.x += (destx - v.x) * drag;
      v.y += (desty - v.y) * drag;


    });

    ball.position = mouse
    // console.log(ball.position, "ball")




  });









  //  DRAG -==--=-==--==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  var $window = $(window)
    .bind('mousemove', function (e) {
      mouse.x = -window.innerWidth / 2 + e.clientX;
      mouse.y = -window.innerHeight / 2 + e.clientY;
      // console.log( -1*(Math.atan2(mouse.y, mouse.x)*(180/Math.PI)))
      shadow.offset.x = 5 * radius * (mouse.x - two.width / 2) / two.width;
      shadow.offset.y = 5 * radius * (mouse.y - two.height / 2) / two.height;
    })
    .bind('touchstart', function () {
      e.preventDefault();
      return false;
    })
    .bind('touchmove', function (e) {
      e.preventDefault();
      var touch = e.originalEvent.changedTouches[0];
      mouse.x = touch.pageX;
      mouse.y = touch.pageY;
      shadow.offset.x = 5 * radius * (mouse.x - two.width / 2) / two.width;
      shadow.offset.y = 5 * radius * (mouse.y - two.height / 2) / two.height;
      return false;
    });

})