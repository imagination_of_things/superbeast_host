class ParticleSystem {
    constructor(two, TwoPath) {
        TwoPath.join = "butt"
        this.origin = TwoPath.children[0].clone()
        console.log("origin", this.origin)
        this.displayed = TwoPath.children[0]
        this.two = two
        this.time = 0
        this.amplitude = 0.01;
        this.direction = 180;
        this.theta = 0
        this.amplitude = 20
        this.oldPos = []
        //   this.parent.add(this.displayed)
        //   this.parent.center()

    }
    lerpByDistance(VectorA, VectorB, x) {
        let P = new Two.Vector()
        P.sub(VectorB, VectorA);
        P.normalize();

        P = P.multiplyScalar(x)
        P.addSelf(VectorA)

        return P;
    }

    lerp(x, y, r) {
        return x + ((y - x) * r)
    }


    map_range(value, low1, high1, low2, high2) {
        return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
    }

    isInThetaLimits(theta, limit1, limit2) {

        if (theta * theta < limit1)
            this.theta = Math.sqrt(limit1)
    }

    run(mouse, time) {
        this.theta = Math.atan2(mouse.y, mouse.x) + 2 * Math.PI
        console.log(this.displayed.vertices[4].distanceTo(mouse))

        // this.theta = this.map_range(this.theta, Math.PI, 2*Math.PI, 7, 9.4)

        // this.isInThetaLimits(this.theta, 6)


        this.amplitude = this.map_range(this.displayed.vertices[4].distanceTo(mouse), 0, window.innerWidth/2, 70, 0.1)

        // console.log("system run")
        for (let i = 5; i < this.displayed.vertices.length - 5; i++) {

            if ((i) % 2 === 0) {
                let v = this.displayed.vertices[i]
                this.oldPos[i] = v

                let o = this.origin.vertices[i]
                // console.log(o.x)
                // let half = v.addSelf(mouse).multiplyScalar(0.05)

                v.x = this.lerp(o.x + this.amplitude * Math.cos(this.theta + noise.perlin2(time * 0.01 * v.x, time * v.x * 0.01*this.amplitude) * Math.PI/5), this.oldPos[i].x, 0.8);
                v.y = this.lerp(o.y + this.amplitude * Math.sin(this.theta + noise.perlin2(time * 0.01 * v.y, time * v.y * 0.01*this.amplitude) * Math.PI/5), this.oldPos[i].y, 0.85);

                this.oldPos[i] = v






                // console.log("distance", v.distanceTo(o))
            }
        }


        this.time += 1

        if (this.time === 100) {
            // this.reset()
            this.time = 0
        }


    }

    reset() {
        for (let i = 0; i < this.displayed.vertices.length; i++) {
            let v = this.displayed.vertices[i]
            let o = this.origin.vertices[i]
            v.x = o.x
            v.y = o.y
        }
    }

    // A function to apply a force to all Particles
    applyForce(f) {

    }

    applyRepeller(r) {

    }

}